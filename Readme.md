![Docky](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/docky-logo.png)


Disclaimer
========

I am not responsible if you cause any damage to your Nintendo Switch or your dock, so proceed at your own risk. With that out of the way.

What is Docky?
===

**Update 5/17/2017:** Check out Docky review via youtuber and thingiverse user Rawdilz: https://www.youtube.com/watch?v=jU5JQAN596U

**Update 5/9/2017:** Added Docky no cover versions, Docky with no hinges and no need to use the cover. **(This was a user request I'm not sure about stability of this model and I have not printed it, use at your own risk)**

**Update 5/1/2017:** Added power led indicator and front bevel to switch docking area, rebuilt 3D model from scratch with better proportions.

Docky is a pocket size custom 3D model you can print and use as an enclosure for the internal components of your OEM Nintendo Switch Dock.

Dimensions are as follows:

- Docky Top:
  - Width: 67.36mm
  - Height: 20.70mm
  - Depth: 105mm

- Docky Cover:
 - Width: 70.76mm
 -  Height: 20.70mm
 -  Depth: 34.78mm

- Docky Bottom:
  - Width: 61.43mm
  - Height: 10.00mm
  - Depth: 99.37mm

![Docky](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/step-12.JPG)

![Docky Front View](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/step-13.JPG)

![Docky Side Left](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/step-14.JPG)

![Docky Top Right](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/step-15.JPG)

![Docky Rear View](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/step-16.JPG)

![Docky Front Switch](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/step-17.JPG)

![Docky Switch Angle](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/step-18.JPG)

![Docky Left Switch](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/step-19.JPG)

![Docky In Carrying Case](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/step-20.JPG)


### What did you use to build this model?

I used Blender to make this 3D model.

### Files for 3D Printing

Files for 3D printing are in the STL folder:
  - docky-bottom.stl
  - docky-cover-beveled.stl (Docky cover is beveled)
  - docky-cover-embossed.stl (Docky logo is embossed)
  - docky-cover.stl (No logo)
  - docky-top-beveled.stl (Nintendo Switch logo beveled)
  - docky-top-embossed.stl (Nintendo Switch logo embossed)
  - docky-top.stl (No logo, add your own if you want)
  - docky-top-no-cover.stl (No logo and no cover hinges, add your own if you want)
  - docky-top-no-cover-beveled.stl (Nintendo Switch logo beveled and no cover hinges)
  - docky-top-no-cover-embossed.stl (Nintendo Switch logo embossed and no cover hinges)

### Tools and Items Needed
  1. Tri-Point Screwdriver - I recommend [this kit](https://www.amazon.com/TEKTON-2830-Everybit-Precision-Electronic/dp/B009MKGRQA/ref=sr_1_5?ie=UTF8&qid=1492561634&sr=8-5&keywords=macbook+pro+screwdriver+kit)
  2. 1 Nintendo Switch Dock
  3. 9 OEM Nintendo Dock Screws
  4. 2 USB-C OEM Nintendo Dock Screws
  5. Docky STL files

### Instructions on how to open your Nintendo Switch Dock

[![Taking Apart The Nintendo Switch Dock!](http://img.youtube.com/vi/LxUuDh5dOus/sddefault.jpg)](http://www.youtube.com/watch?v=LxUuDh5dOus "Taking Apart The Nintendo Switch Dock!")

### Installing the Motherboard in Docky
  1. You should have all 3 Docky components
  ![Docky Components](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/step-1.JPG)
  2. Place the motherboard and fold a piece of the ribbon and put it under the usb ports so when you secure the motherboard the pressure keeps the ribbon in place. Secure the motherboard in the bottom Docky component with 5 screws.
  ![Docky Motherboard](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/step-2.JPG)
  ![Docky Ribbon Placement](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/step-3.JPG)
  3. Place the led board into the crevice in the bottom component and apply some pressure until it's pretty firm.
  ![Docky Power Led](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/step-4.JPG)
  4. Place usb-c port and tighten the 2 screws.
  ![Docky USB-C Port](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/step-5.JPG)
  5. With the bottom component fully assembled insert the side with the HDMI, usb and power ports into the top Docky component first, apply a little pressure.
  ![Docky Assembly](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/step-6.JPG)
  ![Docky Assembly](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/step-7.JPG)
  6. Proceed to insert the other side with the 2 usb ports by lifting a bit the plastic so you can slide the bottom Docky component into the top component.
  ![Docky Assembly](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/step-8.JPG)
  7. Tighten the bottom Docky component with 4 screws.
  ![Docky Assembly](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/step-9.JPG)
  ![Docky Assembly](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/step-10.JPG)
  8. Install the cover by placing one end into the top Docky component and then sliding the other end until it seats firmly.
  ![Docky Assembly](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/step-11.JPG)
  9. Your shiny new pocket size Docky is ready to go.
  ![Docky Top View](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/step-12.JPG)
  ![Docky Cover Open](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/step-13.JPG)
  ![Docky Cover Open Left](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/step-14.JPG)
  ![Docky Cover Open Right](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/step-15.JPG)
  ![Docky Cover Open Rear](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/step-16.JPG)
  ![Docky Front Switch](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/step-17.JPG)
  ![Docky Switch Angle](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/step-18.JPG)
  ![Docky Left Switch](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/step-19.JPG)
  ![Docky In Carrying Case](https://gitlab.com/benmarte/docky/raw/0ea771bad4a319155a8ea1c41de0d629d8fc698e/assets/step-20.JPG)

### I want one but I don't have a 3D printer

You can use one of these online services to print your Docky and have it mailed to you.

  - [Shapeways](https://www.shapeways.com/)
  - [i.materialise](https://i.materialise.com/)
  - [Sculpteo](https://www.sculpteo.com/en/)
  - [3D Hubs](https://www.3dhubs.com/)

### I want to make changes/improvements to Docky what do I do?

Awesome, just clone this repo make your changes and submit a Pull Request so I can add it as another version. I am not a professional 3D artist so I'm sure there's a lot of improvement that can be made to the model so any help is appreciated.

### How can I contact you?

Your best bet is twitter: [@benmarte](https://twitter.com/benmarte)